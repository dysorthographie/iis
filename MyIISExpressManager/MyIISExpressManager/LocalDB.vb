﻿Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.SqlServer

Public Class LocalDB

    Public Shared Function DoesInstanceExist(path As String, instanceName As String) As Boolean
        ' Créer un processus pour exécuter la commande sqllocaldb info
        Dim process As New Process()
        Dim processStartInfo As New ProcessStartInfo()
        processStartInfo.FileName = $"{path}sqllocaldb"
        processStartInfo.Arguments = "info"
        processStartInfo.RedirectStandardOutput = True
        processStartInfo.UseShellExecute = False
        process.StartInfo = processStartInfo

        ' Démarrer le processus
        process.Start()

        ' Lire la sortie standard
        Dim output As String = process.StandardOutput.ReadToEnd()

        ' Attendre que le processus se termine
        process.WaitForExit()

        ' Vérifier si le nom de l'instance existe dans la sortie
        Return output.Contains(instanceName)
    End Function
    Public Shared Sub EnsureSqlServer2022IsUsed(ByVal sqlServer2022Path As String)
        ' Modifier la variable PATH pour s'assurer que SQL Server 2022 est utilisé
        ModifyPath(sqlServer2022Path)

        ' Afficher un message à l'utilisateur pour redémarrer l'application ou le système
        MessageBox.Show("La variable PATH a été mise à jour. Veuillez redémarrer l'application ou le système pour que les modifications prennent effet.")
    End Sub

    Public Shared Function StartInstance(instanceName As String, localDBPath As String) As Boolean
        Try
            ' Chemin complet vers sqllocaldb.exe
            Dim localDBExe As String = System.IO.Path.Combine(localDBPath, "sqllocaldb.exe")

            ' Vérifier si le fichier sqllocaldb.exe existe
            If Not System.IO.File.Exists(localDBExe) Then
                MessageBox.Show("Le fichier sqllocaldb.exe n'a pas été trouvé dans le chemin spécifié.")
                Return False
            End If

            ' Démarrer l'instance LocalDB
            Dim processStartInfo As New ProcessStartInfo(localDBExe, $"start {instanceName}")
            processStartInfo.UseShellExecute = False
            processStartInfo.CreateNoWindow = True
            Dim process As Process = Process.Start(processStartInfo)
            process.WaitForExit()

            If process.ExitCode = 0 Then
                Return True
            Else
                MessageBox.Show("Échec du démarrage de l'instance LocalDB.")
                Return False
            End If
        Catch ex As Exception
            MessageBox.Show($"Erreur lors du démarrage de l'instance LocalDB: {ex.Message}")
            Return False
        End Try
    End Function

    Public Shared Function StartProcess(processName As String) As Boolean
        Try
            Dim processInfo As New ProcessStartInfo()
            processInfo.FileName = processName
            Process.Start(processInfo)
            Return True
        Catch ex As Exception
            MessageBox.Show("Erreur lors du démarrage du processus : " & ex.Message)
            Return False
        End Try
    End Function
    Public Shared Function StartProcess(processName As String, arguments As String) As Boolean
        Try
            Dim processInfo As New ProcessStartInfo()
            processInfo.FileName = processName
            processInfo.Arguments = arguments
            Process.Start(processInfo)
            Return True
        Catch ex As Exception
            MessageBox.Show("Erreur lors du démarrage du processus : " & ex.Message)
            Return False
        End Try
    End Function

    ' Fonction pour ajouter une instance LocalDB et retourner True si cela a fonctionné
    Public Shared Function AddLocalDBInstance(path As String, instanceName As String) As Boolean
        Try
            ' Créer un processus pour exécuter la commande sqllocaldb create
            Dim process As New Process()
            Dim processStartInfo As New ProcessStartInfo()
            processStartInfo.FileName = $"{path}sqllocaldb"
            processStartInfo.Arguments = $"create {instanceName}"
            processStartInfo.RedirectStandardOutput = True
            processStartInfo.RedirectStandardError = True
            processStartInfo.UseShellExecute = False
            process.StartInfo = processStartInfo

            ' Démarrer le processus
            process.Start()

            ' Lire la sortie standard et la sortie d'erreur
            Dim output As String = process.StandardOutput.ReadToEnd()
            Dim errorOutput As String = process.StandardError.ReadToEnd()

            ' Attendre que le processus se termine
            process.WaitForExit()

            ' Vérifier le code de sortie du processus
            If process.ExitCode = 0 Then
                Return True
            Else
                ' Afficher le message d'erreur en cas d'échec
                Console.WriteLine($"Erreur lors de la création de l'instance LocalDB : {errorOutput}")
                Return False
            End If
        Catch ex As Exception
            ' Afficher toute exception rencontrée
            Console.WriteLine($"Exception : {ex.Message}")
            Return False
        End Try
    End Function
    Public Shared Function IsLocalDBInstanceRunning(instanceName As String) As Boolean
        Try
            ' Créer un processus pour exécuter la commande sqllocaldb info
            Dim process As New Process()
            Dim processStartInfo As New ProcessStartInfo()
            processStartInfo.FileName = "sqllocaldb"
            processStartInfo.Arguments = $"info {instanceName}"
            processStartInfo.RedirectStandardOutput = True
            processStartInfo.RedirectStandardError = True
            processStartInfo.UseShellExecute = False
            process.StartInfo = processStartInfo

            ' Démarrer le processus
            process.Start()

            ' Lire la sortie standard et la sortie d'erreur
            Dim output As String = process.StandardOutput.ReadToEnd()
            Dim errorOutput As String = process.StandardError.ReadToEnd()

            ' Attendre que le processus se termine
            process.WaitForExit()

            ' Vérifier le code de sortie du processus et la sortie standard
            If process.ExitCode = 0 AndAlso Not String.IsNullOrEmpty(output) AndAlso Not output.Contains("does not exist") Then
                Return True
            Else
                ' Afficher le message d'erreur en cas d'échec
                Console.WriteLine($"Erreur lors de la vérification de l'instance LocalDB : {errorOutput}")
                Return False
            End If
        Catch ex As Exception
            ' Afficher toute exception rencontrée
            Console.WriteLine($"Exception : {ex.Message}")
            Return False
        End Try
    End Function
    Public Shared Function NewDatabase(instanceName As String,
                                   databaseName As String,
                                   databaseFilePath As String) As Boolean
        Dim connectionString As String = $"Server=(localdb)\{instanceName};Integrated Security=true;"

        Dim dataFileName As String = $"{databaseFilePath}{databaseName}.mdf"
        Dim logFileName As String = $"{databaseFilePath}{databaseName}_log.ldf"

        Dim sql As String = $"CREATE DATABASE [{databaseName}] " &
                        $"ON PRIMARY (NAME = {databaseName}_Data, FILENAME = '{dataFileName}') " &
                        $"LOG ON (NAME = {databaseName}_Log, FILENAME = '{logFileName}')"

        Using connection As New SqlConnection(connectionString)
            Using command As New SqlCommand(sql, connection)
                connection.Open()
                Try
                    command.ExecuteNonQuery()
                    Return True
                Catch ex As Exception
                    Return False
                End Try

            End Using
        End Using

        Return True
    End Function

    Public Shared Function AttachDatabase(instanceName As String, mdfFilePath As String) As Boolean
        Dim sql As String = $"use master

 SELECT *
FROM sys.databases
WHERE name = '{Path.GetFileNameWithoutExtension(mdfFilePath)}'
IF @@ROWCOUNT= 0  CREATE DATABASE [{Path.GetFileNameWithoutExtension(mdfFilePath)}]
ON (FILENAME = '{mdfFilePath}')
FOR ATTACH_REBUILD_LOG;"

        Using connection As New SqlConnection($"Data Source=(localdb)\{instanceName}")
            With connection
                .Open()
                Using attachCommand As New SqlCommand(sql, connection)
                    Try
                        attachCommand.ExecuteNonQuery()
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try

                End Using
                .Close()

            End With



        End Using
        'use Dim connection1 As String = "Data Source=(localdb)\MSSQLLocalDB;AttachDbFilename=C:
        'End Using
        '\Users\dysorthographie\TEST1.mdf;Integrated Security=True;Connect Timeout=30;"
        '        Dim cn As 
        '        cn.Open()

    End Function
    Shared Function DeleteInstance(Path As String, instanceName As String) As Boolean
        Try
            ' Créer le processus pour exécuter la commande sqllocaldb delete
            Dim process As New Process()
            process.StartInfo.FileName = $"{Path}sqllocaldb"
            process.StartInfo.Arguments = $"delete {instanceName}"
            process.StartInfo.UseShellExecute = False
            process.StartInfo.RedirectStandardOutput = True
            process.Start()

            ' Attendre que le processus se termine et récupérer la sortie
            process.WaitForExit()
            Dim output As String = process.StandardOutput.ReadToEnd()

            ' Vérifier si la suppression s'est bien passée en vérifiant la sortie
            If output.Contains("deleted") Then
                Return True ' La suppression a réussi
            Else
                Return False ' La suppression a échoué
            End If
        Catch ex As Exception
            ' En cas d'erreur, retourner False
            Return False
        End Try
    End Function
    Public Shared Sub ModifyPath(ByVal newPath As String)
        Dim currentPath As String = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User)

        ' Vérifier si le chemin est déjà dans PATH
        If currentPath Is Nothing OrElse Not currentPath.Split(";"c).Contains(newPath) Then
            ' Mettre le nouveau chemin en priorité en le plaçant au début
            Dim updatedPath As String = $"{newPath};{currentPath}"
            Environment.SetEnvironmentVariable("PATH", updatedPath, EnvironmentVariableTarget.User)
        End If
    End Sub

    Public Shared Sub GenerateRestoreBatchFile(ByVal backupPath As String)
        Dim batFilePath As String = Path.Combine(backupPath, "RestorePath.bat")
        Dim originalPath As String = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User)
        Dim batchContent As String = $"@echo off{Environment.NewLine}"
        batchContent &= $"set PATH={originalPath}{Environment.NewLine}"
        batchContent &= $"echo PATH restored to original state.{Environment.NewLine}"
        File.WriteAllText(batFilePath, batchContent)
    End Sub
    Public Shared Sub ExecuteBatchFile(ByVal filePath As String)
        Dim processStartInfo As New ProcessStartInfo()
        processStartInfo.FileName = filePath
        processStartInfo.UseShellExecute = True
        processStartInfo.Verb = "runas" ' Run as administrator (if needed)
        'Process.Start(processStartInfo)
    End Sub
    Public Shared Sub ConfigureLocalDB(dataLocation As String, instanceName As String)
        Dim connectionString As String = $"Server=(localdb)\{instanceName};Integrated Security=true;"
        Dim query As String = $"EXEC sp_configure 'default data location', '{dataLocation}'; RECONFIGURE; " &
                              $"EXEC sp_configure 'default log location', '{dataLocation}'; RECONFIGURE;"

        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(query, connection)
            Try
                connection.Open()
                command.ExecuteNonQuery()
                MessageBox.Show("Configuration du répertoire par défaut réussie.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show($"Erreur: {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Using
    End Sub

    Public Shared Sub ResetLocalDBConfig(dataLocation As String, instanceName As String)
        Dim connectionString As String = $"Server=(localdb)\{instanceName};Integrated Security=true;"
        Dim query As String = $"EXEC sp_configure 'default data location', '{dataLocation}'; RECONFIGURE; " &
                              $"EXEC sp_configure 'default log location', '{dataLocation}'; RECONFIGURE;"

        Using connection As New SqlConnection(connectionString)
            Dim command As New SqlCommand(query, connection)
            Try
                connection.Open()
                command.ExecuteNonQuery()
                MessageBox.Show("Réinitialisation du répertoire par défaut réussie.", "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show($"Erreur: {ex.Message}", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End Using
    End Sub

End Class
