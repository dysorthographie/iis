﻿Imports System
Imports System.Diagnostics
Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Text.Json
Imports System.Windows.Forms
Imports System.Windows.Forms.VisualStyles.VisualStyleElement
'Imports Microsoft.Data.SqlClient
Imports Microsoft.SqlServer
Imports SQLitePCL
'Imports Microsoft.SqlServer.Management.Common
'Imports Microsoft.SqlServer.Management.Smo

Public Class Form1
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Private SqlLite As OdbcSqlLite = New OdbcSqlLite
    Private Sub NotifyIcon1_Click(sender As Object, e As EventArgs) Handles NotifyIcon1.Click
        ' Gérer l'événement de clic sur l'icône
        MessageBox.Show("Icône de notification cliquée!")
    End Sub
    Private iisExpressProcess As Process

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub



    Private Function StartSite(sitePath As String, port As String) As Process
        Try
            'Dim iisExpressPath As String = "C:\Program Files\IIS Express\iisexpress.exe"
            Dim iisExpressPath As String = $"{Path.Combine(Application.StartupPath, "IIS Express")}\iisexpress.exe"
            If Not File.Exists(iisExpressPath) Then
                TextBox1.Text = ("Exécutable IIS Express introuvable.") & Environment.NewLine & TextBox1.Text
                Return Nothing
            End If

            Dim startInfo As New ProcessStartInfo(iisExpressPath) With {
            .Arguments = $"/path:""{sitePath}"" /port:{port}",
            .UseShellExecute = False,
            .RedirectStandardOutput = True,
            .RedirectStandardError = True,
            .CreateNoWindow = True
        }

            Dim iisExpressProcess As New Process With {
            .StartInfo = startInfo
        }

            AddHandler iisExpressProcess.OutputDataReceived, AddressOf OutputHandler
            AddHandler iisExpressProcess.ErrorDataReceived, AddressOf ErrorHandler

            iisExpressProcess.Start()
            iisExpressProcess.BeginOutputReadLine()
            iisExpressProcess.BeginErrorReadLine()

            Return iisExpressProcess

        Catch ex As Exception
            TextBox1.Text = ("Erreur lors du démarrage du site : " & ex.Message) & Environment.NewLine & TextBox1.Text
            Return Nothing
        End Try
    End Function

    'Private Function StartSite() As Boolean
    '    Dim wwwDirectory As String = Path.Combine(Application.StartupPath, "WWW")
    '    Dim port As String = txtPort.Text

    '    If String.IsNullOrEmpty(wwwDirectory) OrElse String.IsNullOrEmpty(port) Then
    '        TextBox1.Text = "Veuillez entrer à la fois le chemin du site et le numéro de port" + TextBox1.Text
    '        Return False
    '    End If

    '    If Not Directory.Exists(wwwDirectory) Then
    '        TextBox1.Text = ("Le répertoire spécifié n'existe pas.") + TextBox1.Text
    '        Return False
    '    End If

    '    iisExpressProcess = StartSite(wwwDirectory, port)
    '    Return True
    'End Function
    Private Sub StopSite()
        Try
            If iisExpressProcess IsNot Nothing AndAlso Not iisExpressProcess.HasExited Then
                iisExpressProcess.Kill()
                iisExpressProcess = Nothing
                ' MessageBox.Show("Site arrêté.")
            Else
                ' MessageBox.Show("Le site n'est pas démarré.")
            End If
        Catch ex As Exception
            TextBox1.AppendText("Erreur lors de l'arrêt du site : " & ex.Message)
        End Try
    End Sub

    Private Function OutputHandler(sender As Object, e As DataReceivedEventArgs) As Boolean
        If Not String.IsNullOrEmpty(e.Data) Then Return False
        Return True
    End Function

    Private Function ErrorHandler(sender As Object, e As DataReceivedEventArgs) As Boolean
        If Not String.IsNullOrEmpty(e.Data) Then Return False
        Return True
    End Function

    'Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs)

    '    IIS.Text = If(IIS.Checked, "IIS Démarré ", "IIS Arrêté")
    '    TextBox1.Text = IIS.Text + Environment.NewLine + TextBox1.Text
    '    If IIS.Checked Then
    '        IIS.Checked = StartSite()
    '        My.Settings.DefaultPort = txtPort.Text.Trim
    '        My.Settings.Save()
    '    Else
    '        StopSite()

    '    End If
    'End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer1.Interval = 500
        Timer1.Start()
        LocalDbInstance.Text = My.Settings.LocalDbInstance
        AutoStartMYSQL.Checked = My.Settings.AutoStartMYSQL
        AutoStartFileZilaS.Checked = My.Settings.AutoStartFileZilaS
        AutoStartLocalDB.Checked = My.Settings.AutoStartLocalDB
        SqlLite.Connection($"{Path.Combine(Application.StartupPath, "IIS Express")}\IIS.sqlite")
        Me.DataGridView1.DataSource = SqlLite.OpenDataTable("select * from IIS")
        Me.DataGridView1.Columns("ID").Visible = False : Me.DataGridView1.Columns("Handle").Visible = False
        Dim btnColumn As New DataGridViewButtonColumn()
        btnColumn.Name = "btnAction"
        btnColumn.HeaderText = "HTTP://"
        btnColumn.Text = "WWW"
        btnColumn.UseColumnTextForButtonValue = True
        DataGridView1.Columns.Add(btnColumn)
        Dim columnIndex As Integer = DataGridView1.Columns("Start/Stop").Index
        Dim columnIndex2 As Integer = DataGridView1.Columns("Démarage Auto").Index
        For Each row As DataGridViewRow In DataGridView1.Rows
            If Not row.IsNewRow Then ' Ignorer la ligne "Nouvelle ligne"
                row.Cells(columnIndex).Value = row.Cells(columnIndex2).Value
            End If
        Next
        'NotifyIcon1.Icon = Me.Icon
        'NotifyIcon1.Text = "Mon Application"
    End Sub

    Private Sub Form1_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        'IIS.Checked = False : MYSQL.Checked = False
        FileZila.Checked = False : FileZilaS.Checked = False : LocalDBCheck.Checked = False
        VSCode.Checked = False
        Dim columnIndex As Integer = DataGridView1.Columns("Start/Stop").Index

        For Each row As DataGridViewRow In DataGridView1.Rows
            If Not row.IsNewRow Then ' Ignorer la ligne "Nouvelle ligne"
                row.Cells(columnIndex).Value = 0
            End If
        Next
        SqlLite.MajDataGridView(DataGridView1)
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'If IsProcessRunning(iisExpressProcess) Then
        '    Me.StatuSite.ImageIndex = 1 ' Vert
        'Else
        '    Me.StatuSite.ImageIndex = 0 'Rouge
        '    IIS.Checked = False
        'End If
        If IsAppliRunning("mysqld") Then
            StatuMySql.ImageIndex = 1 ' Vert
        Else
            StatuMySql.ImageIndex = 0 'Rouge
            MYSQL.Checked = False
        End If
        If IsAppliRunning("FileZilla Server Interface") Then
            StatuFileZilaS.ImageIndex = 1 ' Green
        Else
            StatuFileZilaS.ImageIndex = 0 'Rouge
            FileZilaS.Checked = False
        End If
        If IsAppliRunning("FileZillaPortable") Then
            StatuFileZila.ImageIndex = 1 ' Green
        Else
            StatuFileZila.ImageIndex = 0 'Rouge
            FileZila.Checked = False
        End If
        If IsAppliRunning("sqlservr") Then
            StatuLocalDB.ImageIndex = 1 ' Green
        Else
            StatuLocalDB.ImageIndex = 0 'Rouge
            LocalDBCheck.Checked = False
        End If
        If IsAppliRunning("Code") Then
            StatuVSCode.ImageIndex = 1 ' Vert
        Else
            StatuVSCode.ImageIndex = 0 'Rouge
            VSCode.Checked = False
        End If

    End Sub
    Function IsAppliRunning(processName As String) As Boolean
        Dim processes() As Process = Process.GetProcessesByName(processName)
        Return processes.Length > 0
    End Function
    Private Function IsProcessRunning(process As Process) As Boolean
        If process Is Nothing Then Return False
        Try
            ' Check if the process has exited
            Return Not process.HasExited
        Catch ex As Exception
            ' If there's an exception, the process might be invalid or not running
            TextBox1.Text = ("Erreur lors de la vérification du processus : " & ex.Message) & Environment.NewLine & TextBox1.Text
            Return False
        End Try
    End Function
    Private Function KillProcess(process As Process) As Boolean
        Try
            If process IsNot Nothing Then
                If Not process.HasExited Then
                    process.Kill()
                    process.WaitForExit()  ' Attendre que le processus soit terminé (optionnel)
                End If
                Return True
            Else
                TextBox1.Text = "Le processus est invalide ou n'existe pas." & Environment.NewLine & TextBox1.Text
                Return False
            End If
        Catch ex As Exception
            TextBox1.Text = "Erreur lors de l'arrêt du processus : " & ex.Message & Environment.NewLine & TextBox1.Text
            Return False
        End Try
    End Function


    Sub KillApplication(processName As String)
        Dim processes() As Process = Process.GetProcessesByName(processName)
        For Each proc As Process In processes
            Try
                proc.Kill()
                proc.WaitForExit() ' Attendre que le processus se termine
            Catch ex As Exception
                TextBox1.AppendText("Erreur lors de la fermeture du processus : " & ex.Message)
            End Try
        Next
    End Sub
    Private Sub MYSQL_CheckedChanged(sender As Object, e As EventArgs) Handles MYSQL.CheckedChanged

        If MYSQL.Checked Then
            MyIISExpressManager.LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "MySql")}\bin\mysqld.exe")
        Else

            KillApplication("mysqld")
            KillApplication("MySQLAdministrator")
            KillApplication("MySQLQueryBrowser")

        End If
        MYSQL.Text = If(MYSQL.Checked, "MySql Démarré ", "MySql Arrêté")
        TextBox1.Text = MYSQL.Text + Environment.NewLine + TextBox1.Text
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If MYSQL.Checked Then
            MyIISExpressManager.LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "MySQL Administrator")}\MySQLAdministrator.exe")
        End If
    End Sub

    'Private Sub btnOpenBrowser_Click(sender As Object, e As EventArgs)
    '    If IsNumeric(txtPort.Text) And IIS.Checked Then
    '        OpenUrlInDefaultBrowser($"http://localhost:{txtPort.Text}/")
    '    End If
    'End Sub
    Private Sub OpenUrlInDefaultBrowser(url As String)
        Try
            Process.Start(url)
        Catch ex As Exception
            ' Pour certaines versions de .NET, Process.Start(url) peut lancer une exception
            ' si le navigateur par défaut n'est pas correctement configuré.
            ' On essaie une méthode alternative avec l'utilisation de ShellExecute pour plus de compatibilité.
            Try
                Dim psi As New ProcessStartInfo
                psi.FileName = url
                psi.UseShellExecute = True
                Process.Start(psi)
            Catch innerEx As Exception
                TextBox1.AppendText("Erreur lors de l'ouverture du navigateur par défaut : " & innerEx.Message)
            End Try
        End Try
    End Sub
    'Private Sub txtPort_TextChanged(sender As Object, e As EventArgs)
    '    IIS.Checked = False
    'End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If MYSQL.Checked Then
            MyIISExpressManager.LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "MySQL Query Browser")}\MySQLQueryBrowser.exe")
        End If
    End Sub
    Private Sub CheckBox3_CheckedChanged(sender As Object, e As EventArgs) Handles FileZilaS.CheckedChanged
        FileZilaS.Text = If(FileZilaS.Checked, "FileZila Serveur Démarré ", "FileZila Serveur Arrêté")
        If FileZilaS.Checked Then
            MyIISExpressManager.LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "FileZilla Server")}\FileZilla Server Interface.exe")
        Else
            KillApplication("FileZilla Server Interface")
        End If
        TextBox1.Text = FileZilaS.Text + Environment.NewLine + TextBox1.Text
    End Sub
    Private Sub LocalDBCheck_CheckedChanged(sender As Object, e As EventArgs) Handles LocalDBCheck.CheckedChanged
        Dim LocalPath As String = $"{Path.Combine(Application.StartupPath, "Microsoft SQL Server")}\160\Tools\"
        LocalDbInstance.Enabled = Not LocalDBCheck.Checked
        TextBox3.Enabled = LocalDBCheck.Checked

        If LocalDBCheck.Checked Then
            My.Settings.LocalDbInstance = LocalDbInstance.Text
            My.Settings.Save()
            Dim instanceName = LocalDbInstance.Text
            'StartProcess($"{Path.Combine(Application.StartupPath, "SQL Serveur")}\sqllocaldb.exe start MSSQLLocalDB")
            If Not MyIISExpressManager.LocalDB.DoesInstanceExist(LocalPath, instanceName) Then
                MyIISExpressManager.LocalDB.AddLocalDBInstance(LocalPath, instanceName)
                If Not IsAppliRunning("sqlservr") Then MyIISExpressManager.LocalDB.StartInstance(LocalDbInstance.Text, LocalPath)

                Dim mdfFiles = Directory.GetFiles(Path.Combine(Application.StartupPath, "MeBases"), "*.mdf")

                ' Attacher chaque fichier MDF à l'instance LocalDB
                For Each mdfFile In mdfFiles
                    MyIISExpressManager.LocalDB.AttachDatabase(instanceName, mdfFile)
                Next
            End If


            If Not IsAppliRunning("sqlservr") Then MyIISExpressManager.LocalDB.StartInstance(LocalDbInstance.Text, LocalPath)
            'End If
        Else
            'If IsProcessRunning("sqlbrowser") Then
            KillApplication("sqlservr")
            MyIISExpressManager.LocalDB.DeleteInstance(Path.Combine(Application.StartupPath, "Microsoft SQL Server") + "\160\Tools\", LocalDbInstance.Text)
            KillApplication("DbGate")
            'End If
        End If
        LocalDBCheck.Text = If(LocalDBCheck.Checked, "LocalDB Démarré ", "LocalDB Arrêté")
        TextBox1.Text = LocalDBCheck.Text + Environment.NewLine + TextBox1.Text
    End Sub
    Private Sub StopLocalDB(localDBPath As String, instanceName As String)
        If IsAppliRunning("sqlservr") Then
            MyIISExpressManager.LocalDB.StartInstance(LocalDbInstance.Text, $"{Path.Combine(Application.StartupPath, "Microsoft SQL Server")}\160\Tools\")
        Else
            TextBox1.AppendText("LocalDB instance Is Not running.")
        End If
    End Sub
    Private Sub ExecuteCommand(command As String)
        Dim processInfo As New ProcessStartInfo("cmd.exe", "/c " & command)
        processInfo.CreateNoWindow = True
        processInfo.UseShellExecute = False
        processInfo.RedirectStandardOutput = True
        processInfo.RedirectStandardError = True

        Dim process As Process = Process.Start(processInfo)
        process.WaitForExit()

        Dim output As String = process.StandardOutput.ReadToEnd()
        Dim errorOutput As String = process.StandardError.ReadToEnd()

        TextBox1.AppendText(output & Environment.NewLine)
        If Not String.IsNullOrEmpty(errorOutput) Then
            TextBox1.AppendText("Erreur:  " & errorOutput & Environment.NewLine)
        End If
    End Sub


    Private Sub VSCode_CheckedChanged(sender As Object, e As EventArgs) Handles VSCode.CheckedChanged
        VSCode.Text = If(VSCode.Checked, "VS Code  Démarré ", "VS Code  Arrêté")
        TextBox1.Text = VSCode.Text + Environment.NewLine + TextBox1.Text
        If VSCode.Checked Then
            LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "Microsoft VS Code")}\Code.exe")
        Else
            If IsAppliRunning("Code") Then
                KillApplication("Code")
            End If

        End If
    End Sub

    'Private Sub AutoStartIIS_CheckedChanged_1(sender As Object, e As EventArgs)
    '    AutoStartIIS.ImageIndex = If(AutoStartIIS.Checked, 1, 0)
    '    My.Settings.AutoStartIIS = AutoStartIIS.Checked
    '    My.Settings.Save()
    '    If AutoStartIIS.Checked Then IIS.Checked = True
    'End Sub

    Private Sub AutoStartMYSQL_CheckedChanged(sender As Object, e As EventArgs) Handles AutoStartMYSQL.CheckedChanged
        AutoStartMYSQL.ImageIndex = If(AutoStartMYSQL.Checked, 1, 0)
        My.Settings.AutoStartMYSQL = AutoStartMYSQL.Checked
        My.Settings.Save()
        If AutoStartMYSQL.Checked Then MYSQL.Checked = True
    End Sub

    Private Sub AutoStartFileZilaS_CheckedChanged(sender As Object, e As EventArgs) Handles AutoStartFileZilaS.CheckedChanged
        AutoStartFileZilaS.ImageIndex = If(AutoStartFileZilaS.Checked, 1, 0)
        My.Settings.AutoStartFileZilaS = AutoStartFileZilaS.Checked
        My.Settings.Save()
        If AutoStartFileZilaS.Checked Then FileZilaS.Checked = True
    End Sub

    Private Sub AutoStartLocalDB_CheckedChanged(sender As Object, e As EventArgs) Handles AutoStartLocalDB.CheckedChanged
        AutoStartLocalDB.ImageIndex = If(AutoStartLocalDB.Checked, 1, 0)
        My.Settings.AutoStartLocalDB = AutoStartLocalDB.Checked
        My.Settings.Save()
        If AutoStartLocalDB.Checked Then LocalDBCheck.Checked = True
    End Sub
    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click

        If LocalDBCheck.Checked Then
            LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "dbgate")}\DbGate.exe")
        End If
    End Sub

    Private Sub LocalDbInstance_TextChanged(sender As Object, e As EventArgs) Handles LocalDbInstance.TextChanged
        If Not LocalDBCheck.Checked Then TextBox2.Text = $"(LocalDb)\{LocalDbInstance.Text}"
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If LocalDBCheck.Checked And TextBox3.Text.Trim <> "" Then
            LocalDB.NewDatabase(LocalDbInstance.Text, TextBox3.Text, $"{Path.Combine(Application.StartupPath, "MeBases")}\")
            TextBox3.Text = ""
        End If
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Me.Close()
    End Sub
    Private Sub DataGridView1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex >= 0 AndAlso e.RowIndex >= 0 Then
            With DataGridView1
                Dim columnName As String = .Columns(e.ColumnIndex).Name
                Select Case columnName
                    Case "Start/Stop"
                        If .Rows(e.RowIndex).Cells("Start/Stop").Value.ToString = "1" Then
                            Try
                                Dim Rep As String = $"{Path.Combine(Application.StartupPath, "WWW")}\{ .Rows(e.RowIndex).Cells("Nom du Site").Value}"
                                .Rows(e.RowIndex).Cells("Handle").Value = StartSite(Rep, .Rows(e.RowIndex).Cells("Port").Value.ToString).Id
                                TextBox1.Text = $"IIS Démarré http://localhost:{ .Rows(e.RowIndex).Cells("Port").Value}/{Environment.NewLine}{TextBox1.Text}"
                            Catch ex As Exception
                                TextBox1.Text = $"IIS Err http://localhost:{ .Rows(e.RowIndex).Cells("Port").Value}/{Environment.NewLine}{ex.Message}{TextBox1.Text}{Environment.NewLine}"
                            End Try
                            .Rows(e.RowIndex).Cells("port").ReadOnly = True
                            .Rows(e.RowIndex).Cells("Nom du Site").ReadOnly = True
                        Else
                            If .Rows(e.RowIndex).Cells("Handle").Value.ToString.Trim <> "" Then
                                Try
                                    Dim Process As Process = Process.GetProcessById(.Rows(e.RowIndex).Cells("Handle").Value)
                                    KillProcess(Process)
                                    TextBox1.Text = $"IIS Arrête http://localhost:{ .Rows(e.RowIndex).Cells("Port").Value}/{Environment.NewLine}{TextBox1.Text}"
                                Catch ex As Exception
                                    TextBox1.Text = $"IIS Err http://localhost:{ .Rows(e.RowIndex).Cells("Port").Value}/{Environment.NewLine}{ex.Message}{TextBox1.Text}{Environment.NewLine}"
                                End Try

                                .Rows(e.RowIndex).Cells("Handle").Value = DBNull.Value
                                .Rows(e.RowIndex).Cells("port").ReadOnly = False
                                .Rows(e.RowIndex).Cells("Nom du Site").ReadOnly = False

                            End If
                        End If
                    Case "Démarage Auto"
                        If .Rows(e.RowIndex).Cells("Démarage Auto").Value.ToString = "1" Then
                            .Rows(e.RowIndex).Cells("Start/Stop").Value = 1
                        End If


                End Select
            End With
        End If
    End Sub
    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        ' Vérifiez si l'utilisateur a cliqué sur une cellule valide
        If e.ColumnIndex >= 0 AndAlso e.RowIndex >= 0 Then
            With DataGridView1
                Dim columnName As String = .Columns(e.ColumnIndex).Name
                Select Case columnName
                    Case "btnAction"
                        If .Rows(e.RowIndex).Cells("Start/Stop").Value = 1 Then
                            ' Gérer l'action pour la colonne de boutons
                            OpenUrlInDefaultBrowser($"http://localhost:{ .Rows(e.RowIndex).Cells("Port").Value}/")

                        End If
                    Case "Nom du Site"
                        ' Gérer l'action pour la colonne Nom du Site
                        'MessageBox.Show("Nom du Site clicked in row " & e.RowIndex.ToString())
                    Case "Start/Stop"
                        If .Rows(e.RowIndex).Cells("Port").Value.ToString <> "" And .Rows(e.RowIndex).Cells("Nom du Site").Value.ToString <> "" Then
                            .Rows(e.RowIndex).Cells("Start/Stop").Value = If(Int("0" & .Rows(e.RowIndex).Cells("Start/Stop").Value.ToString) = "0", 1, 0)
                        End If
                        .EndEdit()

                        Return
                    Case "Démarage Auto"
                        .Rows(e.RowIndex).Cells("Démarage Auto").Value = If(Int("0" & .Rows(e.RowIndex).Cells("Démarage Auto").Value.ToString) = "0", 1, 0)
                        .EndEdit()
                    Case "Port"
                        ' Gérer l'action pour la colonne Port
                        ' MessageBox.Show("Port clicked in row " & e.RowIndex.ToString())

                End Select
            End With
        End If
    End Sub




    Private Sub DataGridView1_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles DataGridView1.DataError

        e.ThrowException = False
        e.Cancel = True
    End Sub

    Private Sub FileZila_CheckedChanged(sender As Object, e As EventArgs) Handles FileZila.CheckedChanged
        FileZila.Text = If(FileZila.Checked, "FileZila Client Démarré ", "FileZila Client Arrêté")
        If FileZila.Checked Then
            MyIISExpressManager.LocalDB.StartProcess($"{Path.Combine(Application.StartupPath, "FileZillaPortable")}\FileZillaPortable.exe")
        Else
            KillApplication("FileZillaPortable")
        End If
        TextBox1.Text = FileZila.Text + Environment.NewLine + TextBox1.Text
    End Sub
End Class

Public Class ProcessInfo
    Public Property Id As Integer
    Public Property Name As String

    Public Sub New(id As Integer, name As String)
        Me.Id = id
        Me.Name = name
    End Sub
End Class

Public Class EnvironmentHelper
    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Private Shared Function SendMessageTimeout(ByVal hWnd As IntPtr, ByVal Msg As UInteger, ByVal wParam As IntPtr, ByVal lParam As String, ByVal fuFlags As UInteger, ByVal uTimeout As UInteger, ByRef lpdwResult As IntPtr) As IntPtr
    End Function

    Private Const HWND_BROADCAST As Integer = &HFFFF
    Private Const WM_SETTINGCHANGE As Integer = &H1A
    Private Const SMTO_ABORTIFHUNG As Integer = &H2

    Public Shared Sub AddToPath(newPath As String, TextBox1 As System.Windows.Forms.TextBox)
        If DirectoryExistsInPath(newPath) Then Return
        ' Vérifier si le chemin est valide
        If String.IsNullOrEmpty(newPath) Then
            Throw New ArgumentException("Le chemin ne peut pas être vide.", NameOf(newPath))
        End If

        ' Obtenir la variable d'environnement PATH actuelle
        Dim currentPath As String = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User)

        ' Ajouter le nouveau chemin à la variable PATH si ce n'est pas déjà présent
        If Not currentPath.Contains(newPath) Then
            Dim updatedPath As String = currentPath & newPath & ";"
            Environment.SetEnvironmentVariable("PATH", updatedPath, EnvironmentVariableTarget.User)
            SendMessageTimeout(New IntPtr(HWND_BROADCAST), WM_SETTINGCHANGE, IntPtr.Zero, "Environment", SMTO_ABORTIFHUNG, 1000, IntPtr.Zero)
            TextBox1.AppendText("Le chemin a été ajouté à la variable PATH.")
        Else
            TextBox1.AppendText("Le chemin existe déjà dans la variable PATH.")
        End If
    End Sub

    Public Shared Sub RemoveFromPath(pathToRemove As String, TextBox1 As System.Windows.Forms.TextBox)
        If Not DirectoryExistsInPath(pathToRemove) Then Return

        ' Obtenir la variable d'environnement PATH actuelle
        Dim currentPath As String = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User)

        ' Supprimer le chemin spécifié de la variable PATH
        Dim updatedPath As String = currentPath.Replace(pathToRemove & ";", "")
        Environment.SetEnvironmentVariable("PATH", updatedPath, EnvironmentVariableTarget.User)
        SendMessageTimeout(New IntPtr(HWND_BROADCAST), WM_SETTINGCHANGE, IntPtr.Zero, "Environment", SMTO_ABORTIFHUNG, 1000, IntPtr.Zero)
        TextBox1.AppendText("Le chemin a été supprimé de la variable PATH.") ', "Succès", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Public Shared Function DirectoryExistsInPath(directoryPath As String) As Boolean
        ' Obtenir la variable d'environnement PATH actuelle
        Dim pathVariable As String = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User)

        ' Diviser la variable PATH en une liste de chemins
        Dim paths As String() = pathVariable.Split(";"c)

        ' Vérifier si le répertoire recherché est présent dans la liste des chemins
        For Each path In paths
            If String.Equals(path, directoryPath, StringComparison.OrdinalIgnoreCase) Then
                Return True
            End If
        Next

        ' Si le répertoire n'est pas trouvé dans la variable PATH
        Return False
    End Function



End Class
