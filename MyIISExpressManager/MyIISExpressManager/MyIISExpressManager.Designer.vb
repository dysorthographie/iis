﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        components = New ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Timer1 = New Timer(components)
        MYSQL = New CheckBox()
        StatuMySql = New Label()
        ImageList1 = New ImageList(components)
        Button1 = New Button()
        Button2 = New Button()
        StatuFileZilaS = New Label()
        FileZilaS = New CheckBox()
        StatuLocalDB = New Label()
        LocalDBCheck = New CheckBox()
        PageSetupDialog1 = New PageSetupDialog()
        PictureBox1 = New PictureBox()
        PictureBox2 = New PictureBox()
        PictureBox3 = New PictureBox()
        ImageList2 = New ImageList(components)
        AutoStartMYSQL = New CheckBox()
        AutoStartFileZilaS = New CheckBox()
        AutoStartLocalDB = New CheckBox()
        Button4 = New Button()
        LocalDbInstance = New TextBox()
        TextBox2 = New TextBox()
        Button5 = New Button()
        TextBox3 = New TextBox()
        PictureBox4 = New PictureBox()
        DataGridView1 = New DataGridView()
        GroupBox1 = New GroupBox()
        StatuVSCode = New Label()
        VSCode = New CheckBox()
        GroupBox2 = New GroupBox()
        GroupBox3 = New GroupBox()
        StatuFileZila = New Label()
        FileZila = New CheckBox()
        TextBox1 = New TextBox()
        CType(PictureBox1, ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox2, ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox3, ComponentModel.ISupportInitialize).BeginInit()
        CType(PictureBox4, ComponentModel.ISupportInitialize).BeginInit()
        CType(DataGridView1, ComponentModel.ISupportInitialize).BeginInit()
        GroupBox1.SuspendLayout()
        GroupBox2.SuspendLayout()
        GroupBox3.SuspendLayout()
        SuspendLayout()
        ' 
        ' Timer1
        ' 
        ' 
        ' MYSQL
        ' 
        MYSQL.Appearance = Appearance.Button
        MYSQL.Cursor = Cursors.Hand
        MYSQL.FlatStyle = FlatStyle.System
        MYSQL.Font = New Font("Segoe UI", 13F, FontStyle.Bold)
        MYSQL.Location = New Point(299, 78)
        MYSQL.Name = "MYSQL"
        MYSQL.Size = New Size(425, 86)
        MYSQL.TabIndex = 5
        MYSQL.Text = "MySql Arrêté"
        MYSQL.TextAlign = ContentAlignment.MiddleCenter
        MYSQL.UseVisualStyleBackColor = True
        ' 
        ' StatuMySql
        ' 
        StatuMySql.BackColor = Color.Transparent
        StatuMySql.ImageKey = "Rouge.png"
        StatuMySql.ImageList = ImageList1
        StatuMySql.Location = New Point(8, 78)
        StatuMySql.Name = "StatuMySql"
        StatuMySql.Size = New Size(86, 86)
        StatuMySql.TabIndex = 6
        ' 
        ' ImageList1
        ' 
        ImageList1.ColorDepth = ColorDepth.Depth32Bit
        ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), ImageListStreamer)
        ImageList1.TransparentColor = Color.Transparent
        ImageList1.Images.SetKeyName(0, "Rouge.png")
        ImageList1.Images.SetKeyName(1, "Vert.png")
        ImageList1.Images.SetKeyName(2, "UnCheck.png")
        ImageList1.Images.SetKeyName(3, "Check.png")
        ' 
        ' Button1
        ' 
        Button1.Cursor = Cursors.Hand
        Button1.Image = My.Resources.Resources.Administrator
        Button1.Location = New Point(1114, 78)
        Button1.Name = "Button1"
        Button1.Size = New Size(111, 106)
        Button1.TabIndex = 7
        Button1.UseVisualStyleBackColor = True
        ' 
        ' Button2
        ' 
        Button2.Cursor = Cursors.Hand
        Button2.Image = CType(resources.GetObject("Button2.Image"), Image)
        Button2.Location = New Point(1465, 78)
        Button2.Name = "Button2"
        Button2.Size = New Size(111, 86)
        Button2.TabIndex = 9
        Button2.UseVisualStyleBackColor = True
        ' 
        ' StatuFileZilaS
        ' 
        StatuFileZilaS.BackColor = Color.Transparent
        StatuFileZilaS.ImageKey = "Rouge.png"
        StatuFileZilaS.ImageList = ImageList1
        StatuFileZilaS.Location = New Point(41, 85)
        StatuFileZilaS.Name = "StatuFileZilaS"
        StatuFileZilaS.Size = New Size(86, 86)
        StatuFileZilaS.TabIndex = 11
        ' 
        ' FileZilaS
        ' 
        FileZilaS.Appearance = Appearance.Button
        FileZilaS.Cursor = Cursors.Hand
        FileZilaS.FlatStyle = FlatStyle.System
        FileZilaS.Font = New Font("Segoe UI", 13F, FontStyle.Bold)
        FileZilaS.Location = New Point(332, 86)
        FileZilaS.Name = "FileZilaS"
        FileZilaS.Size = New Size(629, 86)
        FileZilaS.TabIndex = 10
        FileZilaS.Text = "FileZila Serveur Arrêté"
        FileZilaS.TextAlign = ContentAlignment.MiddleCenter
        FileZilaS.UseVisualStyleBackColor = True
        ' 
        ' StatuLocalDB
        ' 
        StatuLocalDB.BackColor = Color.Transparent
        StatuLocalDB.ImageKey = "Rouge.png"
        StatuLocalDB.ImageList = ImageList1
        StatuLocalDB.Location = New Point(8, 238)
        StatuLocalDB.Name = "StatuLocalDB"
        StatuLocalDB.Size = New Size(86, 86)
        StatuLocalDB.TabIndex = 14
        ' 
        ' LocalDBCheck
        ' 
        LocalDBCheck.Appearance = Appearance.Button
        LocalDBCheck.FlatStyle = FlatStyle.System
        LocalDBCheck.Font = New Font("Segoe UI", 13F, FontStyle.Bold)
        LocalDBCheck.Location = New Point(299, 238)
        LocalDBCheck.Name = "LocalDBCheck"
        LocalDBCheck.Size = New Size(522, 86)
        LocalDBCheck.TabIndex = 13
        LocalDBCheck.Text = "LocalDB Arrêté"
        LocalDBCheck.TextAlign = ContentAlignment.MiddleCenter
        LocalDBCheck.UseVisualStyleBackColor = True
        ' 
        ' PictureBox1
        ' 
        PictureBox1.BackColor = Color.Transparent
        PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), Image)
        PictureBox1.Location = New Point(1642, 2)
        PictureBox1.Name = "PictureBox1"
        PictureBox1.Size = New Size(250, 250)
        PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox1.TabIndex = 17
        PictureBox1.TabStop = False
        ' 
        ' PictureBox2
        ' 
        PictureBox2.BackColor = Color.Transparent
        PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), Image)
        PictureBox2.Location = New Point(0, -14)
        PictureBox2.Name = "PictureBox2"
        PictureBox2.Size = New Size(250, 250)
        PictureBox2.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox2.TabIndex = 18
        PictureBox2.TabStop = False
        ' 
        ' PictureBox3
        ' 
        PictureBox3.BackColor = Color.Transparent
        PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), Image)
        PictureBox3.Location = New Point(256, -14)
        PictureBox3.Name = "PictureBox3"
        PictureBox3.Size = New Size(1382, 250)
        PictureBox3.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox3.TabIndex = 19
        PictureBox3.TabStop = False
        ' 
        ' ImageList2
        ' 
        ImageList2.ColorDepth = ColorDepth.Depth32Bit
        ImageList2.ImageStream = CType(resources.GetObject("ImageList2.ImageStream"), ImageListStreamer)
        ImageList2.TransparentColor = Color.Transparent
        ImageList2.Images.SetKeyName(0, "UnCheck.png")
        ImageList2.Images.SetKeyName(1, "Check.png")
        ' 
        ' AutoStartMYSQL
        ' 
        AutoStartMYSQL.Appearance = Appearance.Button
        AutoStartMYSQL.AutoSize = True
        AutoStartMYSQL.BackColor = Color.Transparent
        AutoStartMYSQL.CheckAlign = ContentAlignment.MiddleCenter
        AutoStartMYSQL.Cursor = Cursors.Hand
        AutoStartMYSQL.FlatAppearance.BorderSize = 0
        AutoStartMYSQL.FlatStyle = FlatStyle.Flat
        AutoStartMYSQL.ImageIndex = 0
        AutoStartMYSQL.ImageList = ImageList2
        AutoStartMYSQL.Location = New Point(152, 78)
        AutoStartMYSQL.Name = "AutoStartMYSQL"
        AutoStartMYSQL.Size = New Size(86, 86)
        AutoStartMYSQL.TabIndex = 24
        AutoStartMYSQL.UseVisualStyleBackColor = False
        ' 
        ' AutoStartFileZilaS
        ' 
        AutoStartFileZilaS.Appearance = Appearance.Button
        AutoStartFileZilaS.AutoSize = True
        AutoStartFileZilaS.BackColor = Color.Transparent
        AutoStartFileZilaS.CheckAlign = ContentAlignment.MiddleCenter
        AutoStartFileZilaS.Cursor = Cursors.Hand
        AutoStartFileZilaS.FlatAppearance.BorderSize = 0
        AutoStartFileZilaS.FlatStyle = FlatStyle.Flat
        AutoStartFileZilaS.ImageIndex = 0
        AutoStartFileZilaS.ImageList = ImageList2
        AutoStartFileZilaS.Location = New Point(183, 85)
        AutoStartFileZilaS.Name = "AutoStartFileZilaS"
        AutoStartFileZilaS.Size = New Size(86, 86)
        AutoStartFileZilaS.TabIndex = 25
        AutoStartFileZilaS.UseVisualStyleBackColor = False
        ' 
        ' AutoStartLocalDB
        ' 
        AutoStartLocalDB.Appearance = Appearance.Button
        AutoStartLocalDB.AutoSize = True
        AutoStartLocalDB.BackColor = Color.Transparent
        AutoStartLocalDB.CheckAlign = ContentAlignment.MiddleCenter
        AutoStartLocalDB.Cursor = Cursors.Hand
        AutoStartLocalDB.FlatAppearance.BorderSize = 0
        AutoStartLocalDB.FlatStyle = FlatStyle.Flat
        AutoStartLocalDB.ImageIndex = 0
        AutoStartLocalDB.ImageList = ImageList2
        AutoStartLocalDB.Location = New Point(152, 238)
        AutoStartLocalDB.Name = "AutoStartLocalDB"
        AutoStartLocalDB.Size = New Size(86, 86)
        AutoStartLocalDB.TabIndex = 26
        AutoStartLocalDB.UseVisualStyleBackColor = False
        ' 
        ' Button4
        ' 
        Button4.BackgroundImage = CType(resources.GetObject("Button4.BackgroundImage"), Image)
        Button4.BackgroundImageLayout = ImageLayout.Stretch
        Button4.Cursor = Cursors.Hand
        Button4.FlatStyle = FlatStyle.Flat
        Button4.Location = New Point(1602, 156)
        Button4.Name = "Button4"
        Button4.Size = New Size(188, 178)
        Button4.TabIndex = 27
        Button4.UseVisualStyleBackColor = True
        ' 
        ' LocalDbInstance
        ' 
        LocalDbInstance.Location = New Point(880, 293)
        LocalDbInstance.Name = "LocalDbInstance"
        LocalDbInstance.Size = New Size(366, 55)
        LocalDbInstance.TabIndex = 28
        ' 
        ' TextBox2
        ' 
        TextBox2.BackColor = Color.White
        TextBox2.Font = New Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        TextBox2.Location = New Point(880, 238)
        TextBox2.Name = "TextBox2"
        TextBox2.ReadOnly = True
        TextBox2.Size = New Size(366, 55)
        TextBox2.TabIndex = 29
        ' 
        ' Button5
        ' 
        Button5.Cursor = Cursors.Hand
        Button5.Font = New Font("Segoe UI", 9F, FontStyle.Bold, GraphicsUnit.Point, CByte(0))
        Button5.Location = New Point(1276, 238)
        Button5.Name = "Button5"
        Button5.Size = New Size(300, 55)
        Button5.TabIndex = 30
        Button5.Text = "Nouvelle base"
        Button5.UseVisualStyleBackColor = True
        ' 
        ' TextBox3
        ' 
        TextBox3.Enabled = False
        TextBox3.Location = New Point(1282, 293)
        TextBox3.Name = "TextBox3"
        TextBox3.Size = New Size(300, 55)
        TextBox3.TabIndex = 31
        ' 
        ' PictureBox4
        ' 
        PictureBox4.Cursor = Cursors.Hand
        PictureBox4.Image = My.Resources.Resources._Exit
        PictureBox4.Location = New Point(1653, 1633)
        PictureBox4.Name = "PictureBox4"
        PictureBox4.Size = New Size(146, 207)
        PictureBox4.SizeMode = PictureBoxSizeMode.StretchImage
        PictureBox4.TabIndex = 32
        PictureBox4.TabStop = False
        ' 
        ' DataGridView1
        ' 
        DataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        DataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridView1.Location = New Point(-17, 54)
        DataGridView1.Name = "DataGridView1"
        DataGridView1.RowHeadersWidth = 120
        DataGridView1.Size = New Size(1877, 431)
        DataGridView1.TabIndex = 35
        ' 
        ' GroupBox1
        ' 
        GroupBox1.Controls.Add(StatuVSCode)
        GroupBox1.Controls.Add(DataGridView1)
        GroupBox1.Controls.Add(VSCode)
        GroupBox1.Location = New Point(12, 281)
        GroupBox1.Name = "GroupBox1"
        GroupBox1.Size = New Size(1865, 638)
        GroupBox1.TabIndex = 36
        GroupBox1.TabStop = False
        GroupBox1.Text = "IIS"
        ' 
        ' StatuVSCode
        ' 
        StatuVSCode.BackColor = Color.Transparent
        StatuVSCode.ImageKey = "Rouge.png"
        StatuVSCode.ImageList = ImageList1
        StatuVSCode.Location = New Point(8, 534)
        StatuVSCode.Name = "StatuVSCode"
        StatuVSCode.Size = New Size(86, 86)
        StatuVSCode.TabIndex = 38
        ' 
        ' VSCode
        ' 
        VSCode.Appearance = Appearance.Button
        VSCode.Cursor = Cursors.Hand
        VSCode.FlatStyle = FlatStyle.System
        VSCode.Font = New Font("Segoe UI", 13F, FontStyle.Bold)
        VSCode.Location = New Point(299, 534)
        VSCode.Name = "VSCode"
        VSCode.Size = New Size(522, 86)
        VSCode.TabIndex = 37
        VSCode.Text = "VS Code Arrêté"
        VSCode.TextAlign = ContentAlignment.MiddleCenter
        VSCode.UseVisualStyleBackColor = True
        ' 
        ' GroupBox2
        ' 
        GroupBox2.Controls.Add(StatuMySql)
        GroupBox2.Controls.Add(MYSQL)
        GroupBox2.Controls.Add(TextBox3)
        GroupBox2.Controls.Add(Button4)
        GroupBox2.Controls.Add(TextBox2)
        GroupBox2.Controls.Add(Button1)
        GroupBox2.Controls.Add(Button5)
        GroupBox2.Controls.Add(Button2)
        GroupBox2.Controls.Add(AutoStartMYSQL)
        GroupBox2.Controls.Add(LocalDbInstance)
        GroupBox2.Controls.Add(StatuLocalDB)
        GroupBox2.Controls.Add(LocalDBCheck)
        GroupBox2.Controls.Add(AutoStartLocalDB)
        GroupBox2.Location = New Point(12, 934)
        GroupBox2.Name = "GroupBox2"
        GroupBox2.Size = New Size(1865, 400)
        GroupBox2.TabIndex = 37
        GroupBox2.TabStop = False
        GroupBox2.Text = "Base de données"
        ' 
        ' GroupBox3
        ' 
        GroupBox3.Controls.Add(StatuFileZila)
        GroupBox3.Controls.Add(FileZila)
        GroupBox3.Controls.Add(StatuFileZilaS)
        GroupBox3.Controls.Add(FileZilaS)
        GroupBox3.Controls.Add(AutoStartFileZilaS)
        GroupBox3.Location = New Point(12, 1352)
        GroupBox3.Name = "GroupBox3"
        GroupBox3.Size = New Size(1865, 219)
        GroupBox3.TabIndex = 38
        GroupBox3.TabStop = False
        GroupBox3.Text = "Transfert de Fichiers"
        ' 
        ' StatuFileZila
        ' 
        StatuFileZila.BackColor = Color.Transparent
        StatuFileZila.ImageKey = "Rouge.png"
        StatuFileZila.ImageList = ImageList1
        StatuFileZila.Location = New Point(1018, 85)
        StatuFileZila.Name = "StatuFileZila"
        StatuFileZila.Size = New Size(86, 86)
        StatuFileZila.TabIndex = 27
        ' 
        ' FileZila
        ' 
        FileZila.Appearance = Appearance.Button
        FileZila.Cursor = Cursors.Hand
        FileZila.FlatStyle = FlatStyle.System
        FileZila.Font = New Font("Segoe UI", 13F, FontStyle.Bold)
        FileZila.Location = New Point(1171, 86)
        FileZila.Name = "FileZila"
        FileZila.Size = New Size(629, 86)
        FileZila.TabIndex = 26
        FileZila.Text = "FileZila Client Arrêté"
        FileZila.TextAlign = ContentAlignment.MiddleCenter
        FileZila.UseVisualStyleBackColor = True
        ' 
        ' TextBox1
        ' 
        TextBox1.Location = New Point(20, 1586)
        TextBox1.Multiline = True
        TextBox1.Name = "TextBox1"
        TextBox1.Size = New Size(1568, 309)
        TextBox1.TabIndex = 26
        ' 
        ' Form1
        ' 
        AutoScaleDimensions = New SizeF(20F, 48F)
        AutoScaleMode = AutoScaleMode.Font
        BackColor = Color.White
        BackgroundImageLayout = ImageLayout.Stretch
        ClientSize = New Size(1889, 1907)
        ControlBox = False
        Controls.Add(TextBox1)
        Controls.Add(GroupBox3)
        Controls.Add(GroupBox2)
        Controls.Add(GroupBox1)
        Controls.Add(PictureBox4)
        Controls.Add(PictureBox3)
        Controls.Add(PictureBox2)
        Controls.Add(PictureBox1)
        FormBorderStyle = FormBorderStyle.FixedDialog
        Name = "Form1"
        StartPosition = FormStartPosition.CenterScreen
        CType(PictureBox1, ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox2, ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox3, ComponentModel.ISupportInitialize).EndInit()
        CType(PictureBox4, ComponentModel.ISupportInitialize).EndInit()
        CType(DataGridView1, ComponentModel.ISupportInitialize).EndInit()
        GroupBox1.ResumeLayout(False)
        GroupBox2.ResumeLayout(False)
        GroupBox2.PerformLayout()
        GroupBox3.ResumeLayout(False)
        GroupBox3.PerformLayout()
        ResumeLayout(False)
        PerformLayout()
    End Sub
    Friend WithEvents Timer1 As Timer
    Friend WithEvents MYSQL As CheckBox
    Friend WithEvents StatuMySql As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents StatuFileZilaS As Label
    Friend WithEvents FileZilaS As CheckBox
    Friend WithEvents StatuLocalDB As Label
    Friend WithEvents LocalDBCheck As CheckBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents PageSetupDialog1 As PageSetupDialog
    Protected WithEvents PictureBox1 As PictureBox
    Protected WithEvents PictureBox2 As PictureBox
    Protected WithEvents PictureBox3 As PictureBox
    Friend WithEvents ImageList2 As ImageList
    Friend WithEvents AutoStartMYSQL As CheckBox
    Friend WithEvents AutoStartFileZilaS As CheckBox
    Friend WithEvents AutoStartLocalDB As CheckBox
    Friend WithEvents Button4 As Button
    Friend WithEvents LocalDbInstance As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Button5 As Button
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents StatuVSCode As Label
    Friend WithEvents VSCode As CheckBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents StatuFileZila As Label
    Friend WithEvents FileZila As CheckBox

End Class
